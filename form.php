<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"> </script>  
    <style>
        form{
          width: 450px;
          margin-top: 100px;
          margin-left: auto;
          margin-right: auto;
          border: solid 2px cornflowerblue;
          padding: 40px 80px 40px 80px;
          box-sizing: unset;
        }
        div{
          margin: 12px 0px;
          display: flex;
        }
        .star-class{
            color: red;
        }
        .left-label{
          background: #5eb12fe3;
          padding: 8px 10px;
          border: solid 2px #4e76cc;
          margin-right: 20px;
          margin-bottom: 0;
          width: 30%;
          height: 40%;
          display: block;
          color: white;
        }
        input{
            width: 60%;
            border: 2px solid #4e76cc;
        }
        .name .address{
            border: 2px solid #4e76cc;
        }
        .gender-input{
            display:flex; 
            align-items: center;
        }
        .gender-radio{
            margin-left: 20px!important;
            background: #4e76cc;
        }
        select{
            border: solid 2px #4e76cc;
            width: 35%;
        }
        .submit-btn{
          background: #5eb12fe3;
          padding: 10px 10px;
          border: solid 2px #4e76cc;
          width: 30%;
          display: block;
          color: white;
          font-family: Arial, Helvetica, sans-serif;
          border-radius: 6px;
          margin-top: 20px;
          margin-left: auto;
          margin-right: auto;
        }
        .error{
            color: red;
        }
        .submit-text{
            margin-top: 10px;
        }
        
    </style>
</head>

<?php
    $gender=array('Nam', 'Nữ');
    $khoa=array('' => null, 'MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');

    $name = $gen = $Khoa = $birthday = $address = $image = "";
    $nameErr = $genErr = $khoaErr = $birthdayErr = $imageErr = "";

    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (empty($_POST["name"])) {
            $nameErr = "Hãy nhập tên.";
        } else {
            $name = test_input($_POST["name"]);
        }

        if (empty($_POST["gen"])) {
            $genErr = "Hãy chọn giới tính.";
        } else {
            $gen = test_input($_POST["gen"]);
        }

        if (empty($_POST["khoa"])) {
            $khoaErr = "Hãy chọn phân khoa.";
        } else {
            $Khoa = test_input($_POST["khoa"]);
        }

        if (empty($_POST["birthday"])) {
            $birthdayErr = "Hãy nhập ngày sinh.";
        } else if (!validate($_POST["birthday"])) {
            $birthdayErr = "Hãy nhập ngày sinh đúng định dạng";
        }else {
            $birthday = test_input($_POST["birthday"]);
        }

        $address = test_input($_POST["address"]);
        
        $_POST["image"] = $_FILES;
        $image = $_POST["image"];
        if(empty($_POST["image"])){
            $imageErr = "Hãy upload file ảnh";
        }else{
            $check_image = getimagesize($image["image"]["name"]);
            //Nếu phần tử đầu khác 0 thì file upload là file ảnh
            if($check_image[0] == 0){
                $imageErr = "File uploaded không phải file ảnh!";
            }else{
                $temp = explode(".", $_FILES["image"]["name"]);
                $newfilename = current($temp).'_'.date('YmdHis') . '.' . end($temp);
                if (!file_exists("./upload")) {
                    mkdir("./upload", 0777, true);
                }
                move_uploaded_file($_FILES["image"]["tmp_name"], "./upload/". $newfilename);
            }
        }
        

    }

    function validate($date){
        $dates  = explode('/', $date);
        if (count($dates) == 3) {
            return checkdate($dates[1], $dates[0], $dates[2]);
        }
        return false;
    }

?>
    
<html>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data">
    
    <?php 
    
    if($nameErr == "" && $genErr == "" && $khoaErr == "" &&  $birthdayErr == "" && isset($_POST["submit"])){        
    ?>
        
        <div class="name">
            <label class="left-label">Họ và tên</label>
            <p class="submit-text"><?php echo $name ?></p>
        </div>
        <div class="Gender">
            <label class="left-label">Giới tính</label>
            <p class="submit-text"><?php echo $gen ?></p>
        </div>
        <div class="Khoa">
            <label class="left-label">Phân khoa</label>
            <p class="submit-text"><?php echo $Khoa ?></p> 
        </div>
        <div class="Birthday">
            <label class="left-label">Ngày sinh</label>
            <p class="submit-text"><?php echo $birthday ?></p>
        </div>
        <div class="Address">
            <label class="left-label">Địa chỉ</label>
            <p class="submit-text"><?php echo $address ?></p>
        </div>
        <div class="Image">
            <label class="left-label">Hình ảnh</label>
            <img src="<?php echo $image["image"]["name"]?>" width="200" height="160">
        </div>
        
    <?php //}
            }else {?>
        <span class="error">
        <p><?php
            if ($nameErr != "") echo $nameErr."<br>";
            if ($genErr != "") echo $genErr."<br>";
            if ($khoaErr != "") echo $khoaErr."<br>";
            if ($birthdayErr != "") echo $birthdayErr."<br>";
            if ($imageErr != "") echo $imageErr."<br>";
        ?></p>
        </span>
        <div class="name">
            <label class="left-label">Họ và tên<span class="star-class"> *</span></label>
            <input class="name" type="text" name="name">
        </div>
        <div class="Gender">
            <label class="left-label">Giới tính<span class="star-class"> *</span></label>
            <div>
            <?php for ($i = 0; $i < count($gender); $i++) { ?>
                <label class="gender-input" for="<?php echo $i; ?>">
                    <input type="radio" class="gender-radio" name="gen" value="<?php echo $gender[$i]?>"><?php echo $gender[$i]; ?>   
                </label>
            <?php } ?>
            </div>
        </div>
        <div class="Khoa">
            <label class="left-label">Phân khoa<span class="star-class"> *</span></label>
            <select name="khoa">
                <?php foreach($khoa as $key => $value) {?>
                    <option><?php echo $value; ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="Birthday">
            <label class="left-label">Ngày sinh<span class="star-class"> *</span></label>
            <input type="text" class="datepicker" name="birthday" placeholder="dd/mm/yyyy">
            <script>  
                $('.datepicker').datepicker({  
                    format: 'dd/mm/yyyy'  
            });  
            </script>
        </div>
        <div class="Address">
            <label class="left-label">Địa chỉ</label>
            <input class="address" type="text" name="address" >
        </div>
        <div class="Image">
            <label class="left-label">Hình ảnh</label>
            <input type="file" name="image" style="border: none;">
        </div>
        <input type="submit" name="submit" class="submit-btn" value="Đăng ký">
    <?php } ?>
    </form>
</html>


    
