<head>
    <meta charset="UTF-8">
    <style>
        form{
            width: 500px;
            margin-top: 100px;
            margin-left: auto;
            margin-right: auto;
            border: solid 1px;
            padding: 40px 80px 40px 80px;
        
        }
        div{
            margin: 9px 0px;
            display: flex;
        }
        label{
            width: 25%;
            height: 50%;
            padding: 6 8;
            margin-left: 10px;
            
        }
        .keyword_input, select {
            width: 40%;
            border: solid 1px #003399;
        }
        
        .searchBtn{
            padding: 8px 10px;
            margin-top: 10px;
            margin-left: auto;
            margin-right: auto;
            display: block;
            border: solid 2px #003399;
            border-radius: 5px;
            background: #0066cc;
            color: white;
        }
        .addBtn{
            padding: 6 10;
            margin-top: 40px;
            border: solid 2px #003399;
            border-radius: 5px;
            background: #0066cc;
            color: white;
        }
        table{
            width: 100%;
        }
        th{
            text-align: left;
        }
        td{
            vertical-align: top;
        }
        th, td{
            padding: 8px;
        }
        .deleteBtn, .modifyBtn{
            padding: 5 8 5 8;
            border: solid 1px #0066cc;
            background: #66b0ff;
            color: white;
            margin-left: 10px;
        }
    </style>
</head>
<?php
    $khoa=array('' => null, 'MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
?>
<html>
    <form class="search_students">
        <div class="khoa">
            <label>Khoa</label>
            <select>
                <?php foreach($khoa as $key => $value) {?>
                <option><?php echo $value; ?></option>
                <?php }?>
            </select>
        </div>
        <div class="keyword">
            <label>Từ Khóa</label>
            <input class="keyword_input" type="text">
        </div>
        <input class="searchBtn" type="submit" value="Tìm kiếm">
        <div style="display: flex; justify-content: space-between;">
            <p>Số sinh viên tìm thấy: XXX</p>
            <a href="./form.php"><input type="button" class="addBtn" value="Thêm"></a>
        </div>
        <table>
            <colgroup>
                <col width="80" span="1">
                <col width="300" span="1">
                <col width="300" span="1">
                <col width="200" span="1">
            </colgroup>
            <tr>
                <th>No</th>
                <th>Tên sinh viên</th>
                <th>Khoa</th>
                <th>Action</th>
            </tr>         
            <tr>
                <td>1</td>
                <td>Nguyễn Văn A</td>
                <td>Khoa học máy tính</td>
                <td><button class="deleteBtn">Xóa</button><button class="modifyBtn">Sửa</button></td>
            </tr>
            <tr>
                <td>2</td>
                <td>Trần Thị B</td>
                <td>Khoa học máy tính</td>
                <td><button class="deleteBtn">Xóa</button><button class="modifyBtn">Sửa</button></td>
            </tr>
            <tr>
                <td>3</td>
                <td>Nguyễn Hoàng C</td>
                <td>Khoa học vật liệu</td>
                <td><button class="deleteBtn">Xóa</button><button class="modifyBtn">Sửa</button></td>
            </tr>
            <tr>
                <td>4</td>
                <td>Đinh Quang D</td>
                <td>Khoa học vật liệu</td>
                <td><button class="deleteBtn">Xóa</button><button class="modifyBtn">Sửa</button></td>
            </tr>
        </table>
    </form>

</html>